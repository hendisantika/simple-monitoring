package com.hendisantika.simplemonitoring.Controller

import com.hendisantika.simplemonitoring.Repository.MeasurementRepository
import com.hendisantika.simplemonitoring.measurement.Measurement
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : simple-monitoring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 06.10
 */
@RestController
open class MeasurementController(@Autowired val measurementRepository: MeasurementRepository) {

    @RequestMapping("/measurements")
    fun getMeasurements(
            @RequestParam(value = "url", required = true) url: String,
            @RequestParam(value = "maxResults", required = false, defaultValue = "20") maxResults: Int,
            @RequestParam(value = "dateFrom", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") dateFrom: LocalDate?,
            @RequestParam(value = "dateTo", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") dateTo: LocalDate?): List<Measurement> {

        val measurements = measurementRepository.find(
                url,
                maxResults,
                if (dateFrom == null) null else LocalDateTime.of(dateFrom, LocalTime.of(0, 0)),
                if (dateTo == null) null else LocalDateTime.of(dateTo, LocalTime.of(23, 59)))

        Collections.sort(measurements) { o1, o2 -> o1.timestamp.compareTo(o2.timestamp) }
        return measurements
    }
}