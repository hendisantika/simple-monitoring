package com.hendisantika.simplemonitoring.Repository

import com.hendisantika.simplemonitoring.measurement.Measurement
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : simple-monitoring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 06.06
 */
@Repository
class MeasurementRepository(@PersistenceContext val em: EntityManager) {

    @Transactional
    fun createMeasurement(name: String, url: String, status: String, time: Long) {
        val measurement = Measurement(name = name, url = url, status = status, duration = time, timestamp = LocalDateTime.now())
        em.persist(measurement)
    }

    open fun find(url: String, maxResults: Int, dateFrom: LocalDateTime?, dateTo: LocalDateTime?): List<Measurement> {
        var queryString = "select m from Measurement m where m.url = :url "
        if (dateFrom != null) {
            queryString += " and m.timestamp >= :dateFrom"
        }
        if (dateTo != null) {
            queryString += " and m.timestamp <= :dateTo"
        }
        queryString += " order by m.timestamp desc"

        val q = em.createQuery(queryString, Measurement::class.java)
        q.setParameter("url", url)
        if (dateFrom != null) {
            q.setParameter("dateFrom", dateFrom)
        }
        if (dateTo != null) {
            q.setParameter("dateTo", dateTo)
        }
        q.maxResults = maxResults
        return q.resultList
    }

}
