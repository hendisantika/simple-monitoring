package com.hendisantika.simplemonitoring.measurement

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * Created by IntelliJ IDEA.
 * Project : simple-monitoring
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 06.04
 */
@Entity
data class Measurement(@Id @GeneratedValue var id: Int = 0,
                       var name: String = "",
                       var url: String = "",
                       var status: String = "",
                       var duration: Long? = 0,
                       var timestamp: LocalDateTime = LocalDateTime.now()) {

    val formattedTimestamp: String
        get() = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").format(timestamp)

    val isoTimestamp: String
        get() = DateTimeFormatter.ISO_DATE_TIME.format(timestamp)

}