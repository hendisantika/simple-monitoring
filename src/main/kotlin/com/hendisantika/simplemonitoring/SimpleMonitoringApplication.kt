package com.hendisantika.simplemonitoring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimpleMonitoringApplication

fun main(args: Array<String>) {
    runApplication<SimpleMonitoringApplication>(*args)
}
